/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/CustomEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "xAODTruth/TruthParticle.h"
#include <TRandom3.h>

#include <xAODEventInfo/EventInfo.h>
#include "TopEvent/EventTools.h"
using xAOD::IParticle;

namespace top{
  ///-- Constrcutor --///
  CustomEventSaver::CustomEventSaver() :
    m_randomNumber(0.),
    m_someOtherVariable(0.)
  {
  }
  
  ///-- initialize - done once at the start of a job before the loop over events --///
  void CustomEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
  {
    ///-- Let the base class do all the hard work --///
    ///-- It will setup TTrees for each systematic with a standard set of variables --///
    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);


    
    ///-- Loop over the systematic TTrees and add the custom variables --///
    for (auto systematicTree : treeManagers()) {
      systematicTree->makeOutputVariable(m_randomNumber, "randomNumber");
      systematicTree->makeOutputVariable(m_someOtherVariable,"someOtherVariable");

      systematicTree->makeOutputVariable(m_AntiKt8EMPFlowJets_pt, "AntiKt8EMPFlowJets_pt");
      systematicTree->makeOutputVariable(m_AntiKt8EMPFlowJets_phi, "AntiKt8EMPFlowJets_phi");
      systematicTree->makeOutputVariable(m_AntiKt8EMPFlowJets_eta, "AntiKt8EMPFlowJets_eta");
      systematicTree->makeOutputVariable(m_AntiKt8EMPFlowJets_mass, "AntiKt8EMPFlowJets_mass");

      systematicTree->makeOutputVariable(ExKt2GASubJets_pt, "ExKt2GASubJets_pt");
      systematicTree->makeOutputVariable(ExKt2GASubJets_phi, "ExKt2GASubJets_phi");
      systematicTree->makeOutputVariable(ExKt2GASubJets_eta, "ExKt2GASubJets_eta");
      systematicTree->makeOutputVariable(ExKt2GASubJets_mass, "ExKt2GASubJets_mass");

      systematicTree->makeOutputVariable(GhostBHadronsFinal_pt, "GhostBHadronsFinal_pt");
      systematicTree->makeOutputVariable(GhostBHadronsFinal_phi, "GhostBHadronsFinal_phi");
      systematicTree->makeOutputVariable(GhostBHadronsFinal_eta, "GhostBHadronsFinal_eta");
      systematicTree->makeOutputVariable(GhostBHadronsFinal_mass, "GhostBHadronsFinal_mass");
      systematicTree->makeOutputVariable(GhostBHadronsFinalCount, "GhostBHadronsFinalCount");
      systematicTree->makeOutputVariable(MyGhostBHadronsCount, "MyGhostBHadronsCount");
      systematicTree->makeOutputVariable(GhostBHadronsFinalPt, "GhostBHadronsFinalPt");
      systematicTree->makeOutputVariable(pdgId, "pdgId");


      systematicTree->makeOutputVariable(GhostCHadronsFinal_pt, "GhostCHadronsFinal_pt");
      systematicTree->makeOutputVariable(GhostCHadronsFinal_phi, "GhostCHadronsFinal_phi");
      systematicTree->makeOutputVariable(GhostCHadronsFinal_eta, "GhostCHadronsFinal_eta");
      systematicTree->makeOutputVariable(GhostCHadronsFinal_mass, "GhostCHadronsFinal_mass");
      systematicTree->makeOutputVariable(GhostCHadronsFinalCount, "GhostCHadronsFinalCount");
      systematicTree->makeOutputVariable(MyGhostCHadronsCount, "MyGhostCHadronsCount");
      systematicTree->makeOutputVariable(GhostCHadronsFinalPt, "GhostCHadronsFinalPt");

      systematicTree->makeOutputVariable(ConeExclTausFinal, "ConeExclTausFinal");
      systematicTree->makeOutputVariable(ExKtbb_av3sd0, "ExKtbb_av3sd0");
      systematicTree->makeOutputVariable(ExKtbb_maxsd0,"ExKtbb_maxsd0");

      systematicTree->makeOutputVariable(Vertices_3dsig, "Vertices_3dsig");
      systematicTree->makeOutputVariable(Vertices_eta, "Vertices_eta");
      systematicTree->makeOutputVariable(Vertices_lxy, "Vertices_lxy");
      systematicTree->makeOutputVariable(Vertices_lz, "Vertices_lz");
      systematicTree->makeOutputVariable(Vertices_lzsig, "Vertices_lzsig");
      systematicTree->makeOutputVariable(Vertices_mass, "Vertices_mass");
      systematicTree->makeOutputVariable(Vertices_phi, "Vertices_phi");
      systematicTree->makeOutputVariable(Vertices_pt, "Vertices_pt");
      systematicTree->makeOutputVariable(Vertices_vtxx, "Vertices_vtxx");
      systematicTree->makeOutputVariable(Vertices_vtxy, "Vertices_vxty");
      systematicTree->makeOutputVariable(Vertices_vtxz, "Vertices_vtxz");

      systematicTree->makeOutputVariable(PFlowIndex, "PFlowIndex" );
      systematicTree->makeOutputVariable(LargeJetIndex, "LargeJetIndex" );
      systematicTree->makeOutputVariable(SubjetIndex, "SubjetIndex" );
      systematicTree->makeOutputVariable(VtIndex, "VtIndex" );


      }
  }
  
  ///-- saveEvent - run for every systematic and every event --///
  void CustomEventSaver::saveEvent(const top::Event& event) 
  {
    ///-- set our variables to zero --///
    m_randomNumber = 0.;
    m_someOtherVariable = 0.;
    const xAOD::JetContainer* rcjets = nullptr;
    const xAOD::JetContainer* subjets = nullptr;
    //    const xAOD::JetContainer* kt4jets = nullptr;    

    ExKtbb_av3sd0 = 0.;
    ExKtbb_maxsd0 = 0.;

    pdgId = 0;
    MyGhostBHadronsCount = 0;
    GhostBHadronsFinalCount = 0;
    GhostBHadronsFinalPt = 0.;
    MyGhostCHadronsCount = 0;
    GhostCHadronsFinalCount = 0;
    GhostCHadronsFinalPt = 0;    

    PFlowIndex = 0;
    LargeJetIndex = 0;
    SubjetIndex = 0;
    VtIndex = 0;

    Vertices_3dsig.clear();
    Vertices_eta.clear();
    Vertices_lxy.clear();
    Vertices_lz.clear();
    Vertices_lzsig.clear();
    Vertices_mass.clear();
    Vertices_phi.clear();
    Vertices_pt.clear();
    Vertices_vtxx.clear();
    Vertices_vtxy.clear();
    Vertices_vtxz.clear();


    top::check(evtStore()->retrieve< const xAOD::JetContainer >( rcjets, "AntiKt8EMPFlowJets"), "Failed to retrieve reclustered 0.8 jets");
    top::check(evtStore()->retrieve< const xAOD::JetContainer >( subjets, "AntiKt8EMPFlowExKt2GASubJets"), "Failed to retrieve reclustered 0.8 subjets");
    //    top::check(evtStore()->retrieve< const xAOD::JetContainer >( kt4jets, "AntiKt4EMPFlowJets"), "Failed to retrieve reclustered 0.4 jets");


    //    static SG::AuxElement::ConstAccessor<std::vector<int>> Taus("ConeExclTausFinal");
    static SG::AuxElement::ConstAccessor<float> avs("ExKtbb_av3sd0");
    static SG::AuxElement::ConstAccessor<float> max("ExKtbb_maxsd0");

    static SG::AuxElement::ConstAccessor<int> BCnt("GhostBHadronsFinalCount");
    static SG::AuxElement::ConstAccessor<float> BPt("GhostBHadronsFinalPt");
    static SG::AuxElement::ConstAccessor<int> CCnt("GhostCHadronsFinalCount");
    static SG::AuxElement::ConstAccessor<float> CPt("GhostCHadronsFinalPt");

    static SG::AuxElement::ConstAccessor<int> LRJi("LargeJetLabel");

    static SG::AuxElement::ConstAccessor<std::vector<ElementLink<DataVector<xAOD::IParticle>>>> BHad("GhostBHadronsFinal");


    static SG::AuxElement::ConstAccessor<std::vector<float>> dsig("SoftBVrtClusterTool_MSVTight_Vertices_3dsig");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Veta("SoftBVrtClusterTool_MSVTight_Vertices_eta");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Vlxy("SoftBVrtClusterTool_MSVTight_Vertices_lxy");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Vlz("SoftBVrtClusterTool_MSVTight_Vertices_lz");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Vlzsig("SoftBVrtClusterTool_MSVTight_Vertices_lzsig");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Vmass("SoftBVrtClusterTool_MSVTight_Vertices_mass");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Vphi("SoftBVrtClusterTool_MSVTight_Vertices_phi");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Vpt("SoftBVrtClusterTool_MSVTight_Vertices_pt");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Vvtxx("SoftBVrtClusterTool_MSVTight_Vertices_vtxx");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Vvtxy("SoftBVrtClusterTool_MSVTight_Vertices_vtxy");
    static SG::AuxElement::ConstAccessor<std::vector<float>> Vvtxz("SoftBVrtClusterTool_MSVTight_Vertices_vtxz");


    ///-- Fill them - you should probably do something more complicated --///
    TRandom3 random( event.m_info->eventNumber() );
    m_randomNumber = random.Rndm();    
    m_someOtherVariable = 42;
    Vertices_link.clear();

    GhostBHadronsFinal_pt.clear();
    GhostBHadronsFinal_phi.clear();
    GhostBHadronsFinal_eta.clear();
    GhostBHadronsFinal_mass.clear();




    m_AntiKt8EMPFlowJets_pt.clear();
    m_AntiKt8EMPFlowJets_phi.clear();
    m_AntiKt8EMPFlowJets_eta.clear();
    m_AntiKt8EMPFlowJets_mass.clear();

    for (const auto jet : *rcjets) { m_AntiKt8EMPFlowJets_pt.push_back(jet->pt());    
                                     m_AntiKt8EMPFlowJets_phi.push_back(jet->phi());
                                     m_AntiKt8EMPFlowJets_eta.push_back(jet->eta());
                                     m_AntiKt8EMPFlowJets_mass.push_back(jet->m());
                                   }

    ExKt2GASubJets_pt.clear();
    ExKt2GASubJets_phi.clear();
    ExKt2GASubJets_eta.clear();
    ExKt2GASubJets_mass.clear();

    for (const auto subjet : *subjets) { ExKt2GASubJets_pt.push_back(subjet->pt());
                                         ExKt2GASubJets_phi.push_back(subjet->phi());
                                         ExKt2GASubJets_eta.push_back(subjet->eta());
                                         ExKt2GASubJets_mass.push_back(subjet->m());
                                       }


    int subjet_cnt = 0;
    for (const auto subjet: *subjets) { 

      //     auto Vertice_3dsigs = dsig(*subjet); for (auto Vertice_3dsig: Vertice_3dsigs) std::cout << Vertice_3dsig << std::endl;
      
      auto Vertice_3dsigs = dsig(*subjet);
      for (auto Vertice_3dsig : Vertice_3dsigs) {Vertices_3dsig.push_back(Vertice_3dsig);
	//	std::cout << Vertice_3dsig << std::endl;
	Vertices_link.push_back(subjet_cnt);}

      auto Vertice_etas = Veta(*subjet);
      for (auto Vertice_eta : Vertice_etas) {Vertices_eta.push_back(Vertice_eta);
	//	std::cout << Vertice_eta << std::endl;
      }
      
      auto Vertice_lxys = Vlxy(*subjet);
      for (auto Vertice_lxy : Vertice_lxys) {Vertices_lxy.push_back(Vertice_lxy);
      }

      auto Vertice_lzs = Vlz(*subjet);
      for (auto Vertice_lz : Vertice_lzs) {Vertices_lz.push_back(Vertice_lz);
      }

      auto Vertice_lzsigs = Vlzsig(*subjet);
      for (auto Vertice_lzsig : Vertice_lzsigs) {Vertices_lzsig.push_back(Vertice_lzsig);
      }

      auto Vertice_mass_s = Vmass(*subjet);
      for (auto Vertice_mass : Vertice_mass_s) {Vertices_mass.push_back(Vertice_mass);
      }

      auto Vertice_phis = Vphi(*subjet);
      for (auto Vertice_phi : Vertice_phis) {Vertices_phi.push_back(Vertice_phi);
      }

      auto Vertice_pts = Vpt(*subjet);
      for (auto Vertice_pt : Vertice_pts) {Vertices_pt.push_back(Vertice_pt);
      }

      auto Vertice_vtxxs = Vvtxx(*subjet);
      for (auto Vertice_vtxx : Vertice_vtxxs) {Vertices_vtxx.push_back(Vertice_vtxx);
      }

      auto Vertice_vtxys = Vvtxy(*subjet);
      for (auto Vertice_vtxy : Vertice_vtxys) {Vertices_vtxy.push_back(Vertice_vtxy);
      }

      auto Vertice_vtxzs = Vvtxz(*subjet);
      for (auto Vertice_vtxz : Vertice_vtxzs) {Vertices_vtxz.push_back(Vertice_vtxz);
      }

      ExKtbb_av3sd0 = avs(*subjet);
      
      ExKtbb_maxsd0 = max(*subjet);

      GhostBHadronsFinalCount = BCnt(*subjet);
      GhostBHadronsFinalPt = BPt(*subjet);
      GhostCHadronsFinalCount = CCnt(*subjet);
      GhostCHadronsFinalPt = CPt(*subjet);
      
      //      std::cout << LRJi(*rcjets)->Index()  << std::endl;

      //      std::cout << "New subjet" << std::endl;
      //      std::cout << "    ATLAS B count " << GhostBHadronsFinalCount << std::endl;
      int bchk = 0;
      int btot = 0;
      std::vector<const IParticle*> bHadrons;
      subjet->getAssociatedObjects<IParticle>("GhostBHadronsFinal", bHadrons);
      //      std::cout << "    Number of B hadrons GA " << bHadrons.size() << std::endl;

      for (const auto* bHadron : bHadrons) {
        const xAOD::TruthParticle* bGhost = dynamic_cast<const xAOD::TruthParticle*>(bHadron);
	//	std::cout << "         B hadron " << btot << " with pT = " << bGhost->pt() << std::endl;
	for (size_t i = 0; i < bGhost->nChildren(); i++) {
	  //	  std::cout << "                child " << i << " with pdgId = " << bGhost->child(i)->pdgId() << std::endl;
	} // close children                            
        if (bGhost->pt() > 5000){
          bool stnCnt = false;
          for (size_t i = 0 ; i < bGhost->nChildren(); i++){
            if (bGhost->child(i)->isBottomHadron()) {
              stnCnt = true;
              break;
            }
          } // close children

          if (!stnCnt){
	    bchk++;   
	    GhostBHadronsFinal_pt.push_back(bGhost->pt());
	    GhostBHadronsFinal_eta.push_back(bGhost->eta());
	    GhostBHadronsFinal_phi.push_back(bGhost->phi());
	    GhostBHadronsFinal_mass.push_back(bGhost->m());
	  } //close pushback
	} // close pT
        btot++;

      } // close B hadron
      //      std::cout << "    My B count " << bchk << std::endl;
      MyGhostBHadronsCount = bchk;



      //      std::cout << "New subjet" << std::endl;
      //      std::cout << "    ATLAS C count " << GhostCHadronsFinalCount << std::endl;
      int cchk = 0;
      int ctot = 0;
      std::vector<const IParticle*> cHadrons;
      subjet->getAssociatedObjects<IParticle>("GhostCHadronsFinal", cHadrons);
      //      std::cout << "    Number of C hadrons GA " << cHadrons.size() << std::endl;

      for (const auto* cHadron : cHadrons) {
        const xAOD::TruthParticle* cGhost = dynamic_cast<const xAOD::TruthParticle*>(cHadron);
	//	std::cout << "         C hadron " << ctot << " with pT = " << cGhost->pt() << std::endl;
        for (size_t i = 0; i < cGhost->nChildren(); i++) {
	  //  std::cout << "                child " << i << " with pdgId = " << cGhost->child(i)->pdgId() << std::endl;
        } // close children                                                                                                                                                                                 
        if (cGhost->pt() > 5000){
          bool stnCnt = false;
          for (size_t i = 0 ; i < cGhost->nChildren(); i++){
            if (cGhost->child(i)->isCharmHadron()) {
              stnCnt = true;
              break;
            }
          } // close children                                                                                                                                                                               

          if (!stnCnt){
            cchk++;
            GhostCHadronsFinal_pt.push_back(cGhost->pt());
            GhostCHadronsFinal_eta.push_back(cGhost->eta());
            GhostCHadronsFinal_phi.push_back(cGhost->phi());
            GhostCHadronsFinal_mass.push_back(cGhost->m());
          } //close pushback                                                                                                                                                                                
        } // close pT                                                                                                                                                                                       
        ctot++;

      } // close C hadron                                                                                                                                                                                  
      //      std::cout << "    My C count " << cchk << std::endl;
      MyGhostCHadronsCount = cchk;









      SubjetIndex = subjet_cnt;
      subjet_cnt++;
      }

















    ///-- Let the base class do all the hard work --///
    top::EventSaverFlatNtuple::saveEvent(event);
  }
  
}
