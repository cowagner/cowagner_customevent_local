/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_CUSTOMEVENTSAVER_H
#define HOWTOEXTENDANALYSISTOP_CUSTOMEVENTSAVER_H

#include "TopAnalysis/EventSaverFlatNtuple.h"

#include "xAODJet/JetContainer.h"
#include "TopConfiguration/TopConfig.h"

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 * 
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */

namespace top{
  class CustomEventSaver : public top::EventSaverFlatNtuple {
    public:
      ///-- Default constrcutor with no arguments - needed for ROOT --///
      CustomEventSaver();
      ///-- Destructor does nothing --///
      virtual ~CustomEventSaver(){}
      
      ///-- initialize function for top::EventSaverFlatNtuple --///
      ///-- We will be setting up out custom variables here --///
      virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;
      
      ///-- Keep the asg::AsgTool happy --///
      virtual StatusCode initialize() override {return StatusCode::SUCCESS;}      
      
      ///-- saveEvent function for top::EventSaverFlatNtuple --///
      ///-- We will be setting our custom variables on a per-event basis --///
      virtual void saveEvent(const top::Event& event) override;
      
    private:
      ///-- Some additional custom variables for the output --///
      //      std::vector<float> m_jet_deltaR;      

      
      std::vector<float>       m_AntiKt8EMPFlowJets_pt;
      std::vector<float>       m_AntiKt8EMPFlowJets_phi;
      std::vector<float>       m_AntiKt8EMPFlowJets_eta;
      std::vector<float>       m_AntiKt8EMPFlowJets_mass;
      
      std::vector<float>       ExKt2GASubJets_pt;
      std::vector<float>       ExKt2GASubJets_eta;
      std::vector<float>       ExKt2GASubJets_phi;
      std::vector<float>       ExKt2GASubJets_mass;

      std::vector<int>         ConeExclTausFinal;
      float       ExKtbb_av3sd0;
      float       ExKtbb_maxsd0;

      std::vector<float>       GhostBHadronsFinal_pt;
      std::vector<float>       GhostBHadronsFinal_eta;
      std::vector<float>       GhostBHadronsFinal_phi;
      std::vector<float>       GhostBHadronsFinal_mass;
      
      int         pdgId;

      int         MyGhostBHadronsCount;
      int         GhostBHadronsFinalCount;
      float       GhostBHadronsFinalPt;

      std::vector<float>       GhostCHadronsFinal_pt;
      std::vector<float>       GhostCHadronsFinal_eta;
      std::vector<float>       GhostCHadronsFinal_phi;
      std::vector<float>       GhostCHadronsFinal_mass;

      int         MyGhostCHadronsCount;
      int         GhostCHadronsFinalCount;
      float       GhostCHadronsFinalPt;

      std::vector<float>       Vertices_3dsig;
      std::vector<int>         Vertices_link;    
      std::vector<float>       Vertices_eta;
      std::vector<float>       Vertices_lxy;
      std::vector<float>       Vertices_lz;
      std::vector<float>       Vertices_lzsig;
      std::vector<float>       Vertices_mass;
      std::vector<float>       Vertices_phi;
      std::vector<float>       Vertices_pt;
      std::vector<float>       Vertices_vtxx;
      std::vector<float>       Vertices_vtxy;
      std::vector<float>       Vertices_vtxz;


      int         PFlowIndex;
      int         LargeJetIndex;
      int         SubjetIndex;
      int         VtIndex;


      

      float m_genFilHT;
      float m_randomNumber;
      float m_someOtherVariable;

      std::shared_ptr<top::TopConfig> m_topconfig;


      ///-- Tell RootCore to build a dictionary (we need this) --///
      ClassDef(top::CustomEventSaver, 0);
  };
}

#endif
