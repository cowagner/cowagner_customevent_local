For setup in a clean directory, run;

    mkdir source build run
    cd source
    git clone https://gitlab.cern.ch/cowagner/cowagner_customevent_local.git
    cd build
    asetup AnalysisBase,21.2.102,here
    cmake ../source
    cmake --build ./
    source */setup.sh
    cd ..
    
After, you can make a setup.sh file with:

    setupATLAS --quiet
    lsetup git
    cd /source
    asetup --restore
    cd ../build
    source */setup.sh
    cd ..